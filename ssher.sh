#!/bin/bash

user=$1
ip=$2

ssh $user@$ip "rm tfo/output.txt"
for site in wsj nytimes amazon TCP_wiki; do
	for rtt in 20 100 200; do
		for tfo in 1 0; do
			echo -e "\n\n\n\n$(date)\n\nNew Iteration! Everything is fiiiiiine. Testing $site $rtt $tfo\n\n\n\n"
			ssh $user@$ip "sudo pkill -int python"
			ssh -X $user@$ip "cd tfo && ./tfo.py $site $rtt $tfo"
		done
	done
done

scp $user@$ip:tfo/output.txt .
