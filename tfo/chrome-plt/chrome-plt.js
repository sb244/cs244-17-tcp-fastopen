// See https://github.com/alex-vv/chrome-load-timer/blob/master/src/timer.js 
(function() {
	window.addEventListener('load', function() {
		setTimeout(function() {
			console.log('chrome-plt: ' + (performance.timing.loadEventEnd - performance.timing.fetchStart));
		}, 0); 
	}); 
})();
