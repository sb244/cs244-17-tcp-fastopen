#! /usr/bin/env python3

import os
import re
import shutil
import subprocess
import time
import json
import sys

def sh(cmd):
    subprocess.run(cmd, shell=True, check=True)

def tail(path, mode, wait=True):
    """We use this function to watch the chrome logs for the page load time.
    It continually waits for a new line to appear at the bottom of the given
    file, yielding that line when it appears.
    """
    while wait and not os.path.exists(path):
        time.sleep(1)
    with open(path, mode) as f:
        while True:
            line = f.readline()
            if line:
                yield line
            else:
                time.sleep(1)

class TFO:

    ARCHIVES = {
            'amazon':   'https://www.amazon.com',
            'nytimes':  'https://www.nytimes.com',
            'wsj':      'https://www.wsj.com',
            'TCP_wiki': 'https://en.wikipedia.org/wiki/Transmission_Control_Protocol'
            }
    # RTTS = [20, 100, 200]
    CHROME_LOG_FILE  = os.path.expanduser('~/.config/google-chrome/chrome_debug.log')
    CHROME_CACHE_DIR = os.path.expanduser('~/.cache/google-chrome')
    CHROME_POST_PLT_RECORD_DELAY = 2
    CHROME_QUIT_SIGNAL = 15
    WEB_PAGE_REPLAY_LAUNCH_DELAY = 10
    TSPROXY_STARTUP_DELAY = 4
    CHROME_SETTINGS_PATH = os.path.expanduser('~/.config/google-chrome/Local State')
    CHROME_LAUNCH_DELAY = 20
    TSPROXY_QUIT_SIGNAL = 2
    UPLOAD_KBPS = 256
    DOWNLOAD_KBPS = 4000
    REPEATS = 12
    # POLIPO_QUIT_SIGNAL = 2

    def asserts(self):
        proc = subprocess.run('whoami', shell=True, stdout=subprocess.PIPE, check=True)
        if proc.stdout == b'root\n':
            raise Exception('TFO must be run as a normal (unprivileged) user')
        proc = subprocess.run('sudo whoami', shell=True, stdout=subprocess.PIPE, check=True)
        if proc.stdout != b'root\n':
            raise Exception('TFO could not sudo to root')
        proc = subprocess.run('which google-chrome', shell=True)
        if proc.returncode != 0:
            raise Exception('Need google-chrome installed')
        
    def download_web_page_replay(self):
        if not os.path.exists('web-page-replay'):
            print('Downloading web-page-replay...')
            sh('git clone https://github.com/chromium/web-page-replay')
            sh('cd web-page-replay && git checkout 9fffa26abd7066ddd98b14976445bb3e11e288d4')

    def download_tsproxy(self):
        if not os.path.exists('tsproxy'):
            print ('Downloading tsproxy...')
            sh('git clone https://github.com/WPO-Foundation/tsproxy')
            sh('cd tsproxy && git checkout a2f578febcd79b751d948f615bbde8f6189fbeed')

    def setup_archive_directory(self):
        if not os.path.isdir('archives'):
            os.mkdir('archives')

    def archive_file(self, site):
        return 'archives/{}.wpr'.format(site)

    def create_web_page_archive(self, site, url):
        print('Creating archive of {}...'.format(url))
        self.launch_web_page_replay('--no-dns_forwarding',
                '--record', self.archive_file(site))
        self.launch_chrome(url,
                '--host-resolver-rules=MAP * 127.0.0.1:443,EXCLUDE localhost')
        plt = self.get_chrome_plt()
        time.sleep(TFO.CHROME_POST_PLT_RECORD_DELAY)
        self.quit_chrome()
        self.quit_web_page_replay()
        if not os.path.exists(self.archive_file(site)):
            raise Exception('web-page-replay archive file not created!')

    def create_archives(self):
        for site, url in TFO.ARCHIVES.items():
            if not os.path.exists(self.archive_file(site)):
                self.create_web_page_archive(site, url)

    def set_tcp_fast_open_enabled(self, enabled):
        self.set_tcp_fast_open_enabled_kernel(enabled)
        self.set_tcp_fast_open_enabled_chrome(enabled)

    def set_tcp_fast_open_enabled_chrome(self, enabled):
        settings_file_read = open(TFO.CHROME_SETTINGS_PATH, 'r')
        settings_obj = json.load(settings_file_read)
        if 'browser' not in settings_obj:
            settings_obj['browser'] = {}
        enabled_experiments = ['enable-tcp-fast-open'] if enabled else []
        settings_obj['browser']['enabled_labs_experiments'] = enabled_experiments
        settings_file_write = open(TFO.CHROME_SETTINGS_PATH, 'w')
        json.dump(settings_obj, settings_file_write)

    def setup_chrome(self):
        if not os.path.exists(TFO.CHROME_SETTINGS_PATH):
            self.launch_chrome('about:blank')
            time.sleep(TFO.CHROME_LAUNCH_DELAY)
            self.quit_chrome()

    def setup(self):
        self.asserts()
        self.setup_chrome()
        self.download_web_page_replay()
        self.download_tsproxy()
        self.setup_archive_directory()
        self.create_archives()

    def run(self, site, rtt, fast_open):
        self.setup()
        self.set_tcp_fast_open_enabled(fast_open)
        result = self.measure_plt(site, TFO.ARCHIVES[site], rtt, fast_open)
        header = site + ',' + str(rtt) + ',' + str(fast_open) + ','
        line = header + ','.join([str(x) for x in result]) + '\n'
        outfile = open('output.txt', 'a')
        outfile.write(line)

    #def measure_all_plts(self, with_fast_open):
    #    all_plts = {}
    #    for site, url in TFO.ARCHIVES.items():
    #        all_plts[url] = {}
    #        for rtt in TFO.RTTS:
    #            all_plts[url][rtt] = self.measure_plt(site, url, rtt, with_fast_open)
    #    return all_plts        

    def set_tcp_fast_open_enabled_kernel(self, enabled):
        sh('sudo sysctl -w net.ipv4.tcp_fastopen={}'.format(0x207 if enabled else 0))
    
    def clear_chrome_cache(self):
        try:
            shutil.rmtree(TFO.CHROME_CACHE_DIR)
        except FileNotFoundError:
            pass

    def clear_chrome_log(self):
        try:
            os.remove(TFO.CHROME_LOG_FILE)
        except FileNotFoundError:
            pass

    def launch_chrome(self, url, *args):
        """Clears chrome cache and log, launches google-chrome from the command
        line and navigates to the given url with the given args
        """
        self.clear_chrome_cache()
        self.clear_chrome_log()
        argv = ['google-chrome', url, '--ignore-certificate-errors',
                '--load-extension=chrome-plt',
                '--enable-logging', '--v=1'] + list(args)
        print('Running {}'.format(' '.join(argv)))
        self.chrome = subprocess.Popen(argv)
        try:
            self.chrome.communicate(timeout=2)
            if self.chrome.returncode != 0:
                raise Exception('google-chrome died')
        except subprocess.TimeoutExpired:
            pass

    def quit_chrome(self):
        while True:
            self.chrome.send_signal(TFO.CHROME_QUIT_SIGNAL)
            try:
                self.chrome.communicate(timeout=5)
                break
            except:
                pass
        self.chrome = None

    def launch_web_page_replay(self, *args):
        argv = ['sudo', 'web-page-replay/replay.py'] + list(args)
        self.replay = subprocess.Popen(argv)
        time.sleep(TFO.WEB_PAGE_REPLAY_LAUNCH_DELAY)

    def quit_web_page_replay(self):
        sh('sudo kill -INT {}'.format(self.replay.pid + 1)) # This is a hack!!!
        self.replay.communicate(timeout=60)
        self.replay = None
        time.sleep(4) # Give web-page-replay time to clean up.

    def get_chrome_plt(self):
        """Uses tail to watch the log file and returns once it finds a plt line
        """
        for line in tail(TFO.CHROME_LOG_FILE, "r"):
            m = re.search(r'\bchrome-plt: ([0-9]+)\b', line)
            if m:
                plt = int(m.group(1))
                print('Page load time: {}'.format(plt))
                return plt

    def launch_tsproxy(self, rtt, up_kbps, down_kbps):
        args = ['python', 'tsproxy/tsproxy.py', '--rtt', str(rtt),
                '--inkbps', str(down_kbps), '--outkbps', str(up_kbps)]
        while True:
            self.tsproxy = subprocess.Popen(args)
            time.sleep(TFO.TSPROXY_STARTUP_DELAY)
            try:
                self.tsproxy.communicate(timeout=1)
                if self.tsproxy.returncode != 0:
                    print('tsproxy died. trying again...')
                    continue
            except subprocess.TimeoutExpired:
                return

    def quit_tsproxy(self):
        while True:
            self.tsproxy.send_signal(TFO.TSPROXY_QUIT_SIGNAL)
            try:
                self.tsproxy.communicate(timeout=5)
                break
            except:
                pass
        self.tsproxy = None
        time.sleep(1) # Give tsproxy time to clean up.

    #def fetch_page_mget(self, url, with_fast_open, with_proxy):
    #    """Clears old weird files from past runs and then runs wget2"""
    #    # print "new fetch_page_mget call"
    #    sh('rm -f ~/.wget*')
    #    args = ['time', 'install/bin/wget2', '-p', '--no-cache', '--check-certificate=0', '--delete-after', '--tls-false-start=0', '--user-agent=Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36']
    #    if with_fast_open:
    #        args.append('--tcp-fastopen=1')
    #    else:
    #        args.append('--tcp-fastopen=0')

    #    if with_proxy:
    #        args.append('--http-proxy=127.0.0.1:8123')
    #    args.append(url)

    #    process = subprocess.Popen(args, stderr=subprocess.PIPE)
    #    output = process.communicate()[1]
    #    output = str(output)
    #    print("mget output: " + output)
    #    match = re.search('([0-9]+):([0-9]+\.[0-9]+)elapsed', output)
    #    if match:
    #        plt = int(match.group(1))*60 + float(match.group(2))
    #        print("page load time was {}".format(plt))
    #        return plt
    #    else:
    #        print("oh no")

    #def launch_polipo(self):
    #    self.polipo = subprocess.Popen(['polipo', '-c', 'polipo-config'])
    #    time.sleep(5)

    #def quit_polipo(self):
    #    while True:
    #        self.polipo.send_signal(TFO.POLIPO_QUIT_SIGNAL)
    #        try:
    #            self.polipo.communicate(timeout=5)
    #            break
    #        except:
    #            pass
    #    self.polipo = None
    #    time.sleep(1) # Give polipo time to clean up, I guess.
    
    def measure_plt(self, site, url, rtt, with_fast_open):
        """Loads the given page from the replay archive repeats times, logs and
        returns all page load times
        """
        self.launch_tsproxy(rtt, TFO.UPLOAD_KBPS, TFO.DOWNLOAD_KBPS)
        self.launch_web_page_replay(self.archive_file(site))
        plts = []
        for i in range(TFO.REPEATS):
            # fetch url using chrome, track rtt
            self.launch_chrome(url, '--proxy-server=socks://localhost:1080')
            plts.append(self.get_chrome_plt())
            self.quit_chrome()
        self.quit_tsproxy()
        self.quit_web_page_replay()
        print(plts)
        return plts

if __name__ == '__main__':
    tfo = TFO()
    site = sys.argv[1]
    rtt = int(sys.argv[2])
    fast_open = bool(int(sys.argv[3]))
    tfo.run(site, rtt, fast_open)
