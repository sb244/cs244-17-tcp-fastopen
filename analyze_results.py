import sys
import math

def get_stats(plts):
	mean = sum(plts)/float(len(plts))
	std_dev = math.sqrt(sum([(val - mean)*(val - mean) for val in plts])/float(len(plts)))
	return (mean, std_dev)

filename = sys.argv[1]
print "analyzing results for " + filename + "..."

file_obj = open(filename, 'r')
all_results = {}
for line in file_obj:
	parsed_line = line.split(',')
	if parsed_line[0] not in all_results:
		all_results[parsed_line[0]] = {}

	if parsed_line[1] not in all_results[parsed_line[0]]:
		all_results[parsed_line[0]][parsed_line[1]] = {}

	if parsed_line[2] not in all_results[parsed_line[0]][parsed_line[1]]:
		all_results[parsed_line[0]][parsed_line[1]][parsed_line[2]] = []

	all_results[parsed_line[0]][parsed_line[1]][parsed_line[2]].extend([int(x) for x in parsed_line[3:]])

output_file = open('output.csv', 'w')
output_file.write('Page,RTT(ms),PLT mean no TFO (ms),PLT std dev no TFO (ms),PLT mean TFO (ms),PLT std dev TFO (ms),Improvement\n')

for site in all_results:
	for rtt in all_results[site]:
		output_line = [site, rtt]

		# no TFO
		normal_mean, normal_std_dev = get_stats(all_results[site][rtt]['False'])
		output_line.append(str(normal_mean))
		output_line.append(str(normal_std_dev))

		# TFO
		tfo_mean, tfo_std_dev = get_stats(all_results[site][rtt]['True'])
		output_line.append(str(tfo_mean))
		output_line.append(str(tfo_std_dev))

		# % improvement
		output_line.append(str((normal_mean - tfo_mean)/float(normal_mean)))

		output_file.write(','.join(output_line) + '\n')

