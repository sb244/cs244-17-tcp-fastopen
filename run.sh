#! /bin/bash

user=$1
ip=$2

cat <<EOF

This script will try to SSH into IP '$ip' as user '$user' multiple times.
Make sure to configure passwordless SSH.

EOF

ping -c 2 $ip || exit 1

#scp -r tfo $user@$ip:tfo
#
#ssh $user@$ip 'bash tfo/setup.sh'

bash ssher.sh $user $ip

python analyze_results.py output.txt

